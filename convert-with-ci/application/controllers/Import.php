<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Import Controller
 *
 * @author TechArise Team
 *
 * @email  info@techarise.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Import_model', 'import');
    }

    // upload xlsx|xls file
    public function index() {
        $data['page'] = 'import';
        $data['title'] = 'Import XLSX | TechArise';
        $this->load->view('import/index', $data);
    }
    public function display() {
        $data['page'] = 'import';
        $data['title'] = 'Import XLSX | TechArise';
        $data['employeeInfo'] = $this->import->employeeList();
        $this->load->view('import/display', $data);
    }    
    // import excel data
    public function save() {
        $this->load->library('excel');
        
        if ($this->input->post('importfile')) {
            $path = ROOT_UPLOAD_IMPORT_PATH;

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('dealername', 'carbrand', 'carmodel', 'stocknumber', 'stockdate','ip_address');
            $makeArray = array('dealername' => 'dealername', 'carbrand' => 'carbrand','carmodel' => 'carmodel', 'stocknumber' => 'stocknumber', 'stockdate' => 'stockdate', 'ip_address' => 'ip_address');
            $SheetDataKey = array();
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    } else {
                        
                    }
                }
            }
            $data = array_diff_key($makeArray, $SheetDataKey);
           
            if (empty($data)) {
                $flag = 1;
            }
            if ($flag == 1) {
                for ($i = 2; $i <= $arrayCount; $i++) {
                    $addresses = array();
                    $dealername = $SheetDataKey['dealername'];
                    $carbrand = $SheetDataKey['carbrand'];
                    $carmodel = $SheetDataKey['carmodel'];
                    $stocknumber = $SheetDataKey['stocknumber'];
                    $stockdate = $SheetDataKey['stockdate'];
                    $ip_address = $SheetDataKey['ip_address'];
                    $dealername = filter_var(trim($allDataInSheet[$i][$dealername]), FILTER_SANITIZE_STRING);
                    $carbrand = filter_var(trim($allDataInSheet[$i][$carbrand]), FILTER_SANITIZE_STRING);
                    $carmodel = filter_var(trim($allDataInSheet[$i][$carmodel]), FILTER_SANITIZE_STRING);
                    $stocknumber = filter_var(trim($allDataInSheet[$i][$stocknumber]), FILTER_SANITIZE_STRING);
                    $stockdate = filter_var(trim($allDataInSheet[$i][$stockdate]), FILTER_SANITIZE_STRING);
                    $ip_address = filter_var(trim($allDataInSheet[$i][$ip_address]), FILTER_SANITIZE_STRING);
                    // ('dealername', 'carbrand', 'carmodel', 'stocknumber', 'stockdate','ip_address');
                    $fetchData[] = array('dealername' => $dealername, 'carbrand' => $carbrand, 'carmodel' => $carmodel, 'stocknumber' => $stocknumber, 'stockdate' => $stockdate,'ip_address'=>$ip_address);
                }              
                $data['employeeInfo'] = $fetchData;
                $this->import->setBatchImport($fetchData);
                $this->import->importData();
            } else {
                echo "Please import correct file";
            }
        }
        $this->load->view('import/display', $data);
        
    }

}
