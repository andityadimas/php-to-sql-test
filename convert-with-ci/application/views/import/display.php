<?php
$this->load->view('templates/header');
?>
<div class="row">
    <div class="col-lg-12">
        <h1>Excel file:Diplay</h1>       
    </div>
</div><!-- /.row -->
<div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
            <tr>
               
                <th class="header">Dealer Name</th>
                <th class="header">Car Brand </th>                           
                <th class="header">Car Model</th>                      
                <th class="header">Stock Number</th>
                <th class="header">Stock Date</th>
                <th class="header">IP Address</th>

            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($employeeInfo) && !empty($employeeInfo)) {
                foreach ($employeeInfo as $key => $element) {
                    ?>
                    <tr>
                        <td><?php echo $element['dealername']; ?></td>   
                        <td><?php echo $element['carbrand']; ?></td> 
                        <td><?php echo $element['carmodel']; ?></td>                       
                        <td><?php echo $element['stocknumber']; ?></td>
                        <td><?php echo $element['stockdate']; ?></td>
                        <td><?php echo $element['ip_address']; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">There is no data.</td>    
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div> 
<div class="row">
    
</div><!-- /.row -->
<?php
$this->load->view('templates/footer');
?>
