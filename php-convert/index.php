<html>
<head>
    <title>Cara Import File CSV atau Excel ke Database MySQL dengan PHP</title>
</head>
<body>
 <div style="border:1px solid #B0C4DE; padding:5px; overflow:auto; width:99%; height:98%;">
      <p><font size="3"><b>Import File CSV atau Import Excel</b></font></p>
        <p>
            <form action="import-excel.php" method="POST" enctype="multipart/form-data" >
                <table border="0">
                    <tr>
                        <td width="25%">Pilih File</td>
                        <td width="75%"><input type="file" name="namafile" maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>
                            <button type="submit" name="upload" value="upload">Import</button>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <br />
                <table border="0">
                    <tr>
                        <td><a href="lihat-hasil.php" type="submit" name="view" value="view">View Tabel</a></td>
                        <td> </td>
                    </tr>
                </table>
            </form>
        </p>
    </div>
</body>
</html>