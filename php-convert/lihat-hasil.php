<html>
<head>
    <title>Import Excel ke Database MySQL dengan PHP</title>
</head>
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 50%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 70%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
<body>
    <div style="border:1px solid #B0C4DE; padding:5px; overflow:auto; width:99%; height:98%;">
        <p><font size=""><b>Hasil Import File CSV atau Import Excel</b></font></p>
        <p><a href="./">Kembali</a></p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
        <p>
            <table id ="myTable" width="1100" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr bgcolor="#FF6600">
                    <th width="10" height="40">Nomor</td> 
                    <th width="10">ID</td> 
                    <th width="20">Dealer name</td> 
                    <th width="20">Car Brand</td>
                    <th width="20">Car Model</td>
                    <th width="20">Srock number</td>  
                    <th width="20">Stock Update</td>
                    <th width="20">IP Addess</td>
                </tr>
                <?php
                    include "koneksi.php";
                    $query=mysql_query("select * from tbl-info");
                    $no=0;    
                    //menampilkan data
                    while($row=mysql_fetch_array($query)){
                ?>
                <tr>
                    <td align="center" height="36"><?php echo $row['nomor'];?></td>
                    <td align="center"><?php echo $row['id'];?></td>
                    <td align="center"><?php echo $row['dealername']; ?></td>
                    <td align="center"><?php echo $row['carbrand'];?></td>
                    <td align="center"><?php echo $row['carmodel']; ?></td>
                    <td align="center"><?php echo $row['stocknumber'];?></td>
                    <td align="center"><?php echo $row['stockdate']; ?></td>
                    <td align="center"><?php echo $row['ip_address'];?></td>
                </tr>
                <?php
                    }
                ?>    
                <tr>
                    <td colspan="4" height="36"> 
                    <?php
                    //jika data tidak ditemukan
                    if(mysql_num_rows($query)==0){
                        echo "<font color=red>Data tidak ditemukan!</font>";
                    }
                    ?>
                    </td>
                </tr> 
            </table>
        </p>
    </div>
    <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</body>
</html>